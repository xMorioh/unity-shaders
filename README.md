# Moriohs-Unity-Shaders

A Set of Shaders for Unitys BiRP which might be quite useful to have. Most of them will have support for the [Amplify Shader Editor](https://assetstore.unity.com/packages/tools/visual-scripting/amplify-shader-editor-68570)

## Shader List:

- [Standard Stochastic](https://gitlab.com/xMorioh/unity-shaders/-/blob/main/Assets/Moriohs%20Unity%20Shaders/Standard%20Stochastic/Standard%20Stochastic.shader)

- [Sea-Water Shader](https://gitlab.com/xMorioh/unity-shaders/-/tree/main/Assets/Moriohs%20Unity%20Shaders/Enviroment/Sea-Water)

- [Triplanar Projection](https://gitlab.com/xMorioh/unity-shaders/-/blob/main/Assets/Moriohs%20Unity%20Shaders/Enviroment/Triplanar%20Projection.shader)


- [Cloud Shader](https://gitlab.com/xMorioh/unity-shaders/-/tree/main/Assets/Moriohs%20Unity%20Shaders/Enviroment/Clouds)


# [See the full list with previews and more information here](https://xmorioh.gitlab.io/Moriohs%20Unity-Shaders.html)
